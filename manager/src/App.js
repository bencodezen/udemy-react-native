import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import Firebase from 'firebase';
import { Header } from './components/common';
import LoginForm from './components/LoginForm';

import reducers from './reducers';

class App extends Component {
	componentWillMount() {
		const config = {
			apiKey: 'AIzaSyBmEhgUwjm8UsdHj8F2FCkpU4YQCRFZFTI',
			authDomain: 'manager-1449e.firebaseapp.com',
			databaseURL: 'https://manager-1449e.firebaseio.com',
			projectId: 'manager-1449e',
			storageBucket: 'manager-1449e.appspot.com',
			messagingSenderId: '61853934787'
		};

		Firebase.initializeApp(config);
	}

	render() {
		return (
			<Provider store={createStore(reducers)}>
				<View>
					<Header headerText="Manager App" />
					<LoginForm />
				</View>
			</Provider>
		);
	}
}

export default App;
