import React, { Component } from 'react';
import { View } from 'react-native';
import Firebase from 'firebase';
import LoginForm from './components/LoginForm';
import { CardSection, Button, Header, Spinner } from './components/common';

class App extends Component {
	state = { loggedIn: null }

	componentWillMount() {
		Firebase.initializeApp({
			apiKey: 'AIzaSyBZEDLoxI-6rlWYC7S5p01ssRHtAfswDok',
			authDomain: 'auth-5f036.firebaseapp.com',
			databaseURL: 'https://auth-5f036.firebaseio.com',
			projectId: 'auth-5f036',
			storageBucket: 'auth-5f036.appspot.com',
			messagingSenderId: '832253986945'
		});

		Firebase.auth().onAuthStateChanged((user) => {
			if (user) {
				this.setState({ loggedIn: true });
			} else {
				this.setState({ loggedIn: false });
			}
		});
	}

	renderContent() {
		switch (this.state.loggedIn) {
			case true:
				return (
					<CardSection>
						<Button onPress={() => Firebase.auth().signOut()}>
							Log Out
						</Button>
					</CardSection>
				);
			case false:
				return <LoginForm />;
			default:
				return (
					<CardSection>
						<Spinner size="large" />
					</CardSection>
				);
		}
	}

	render() {
		return (
			<View>
				<Header headerText="Authentication App" />
				{this.renderContent()}
			</View>
		);
	}
}

export default App;
